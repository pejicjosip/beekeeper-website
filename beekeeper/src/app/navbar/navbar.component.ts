import { Component, HostListener, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  toggleClass: boolean = false;
  constructor(private router: Router) { }

  @HostListener("document:click")
  ngOnInit(): void { }
  clickEvent() {
    this.toggleClass = !this.toggleClass;
  }
}

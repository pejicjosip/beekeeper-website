import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { products } from './product-list';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public productList = products;

  public cartList: any[] = [];

  @Output() cartItems = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  public updateCart(product: any) {
    this.cartList.push(product);
  }
}

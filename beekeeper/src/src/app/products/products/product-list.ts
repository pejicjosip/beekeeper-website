export const products = [
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '130.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Košnica',
    description: 'Dom za Vaše pčele koji omogućuje bezbrižan život.',
    image: 'hive-product.png',
    price: '100.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '130.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '240.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Krov košnice',
    description: 'Krov za košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '75.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Krov košnice',
    description: 'Krov za košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '75.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Krov košnice',
    description: 'Krov za košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '75.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Krov za pčele omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '75.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  }, {
    title: 'Poklopac košnice',
    description: 'Krov za pčele omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '75.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '80.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '90.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  },
  {
    title: 'Poklopac košnice',
    description: 'Poklopac košnice omogućuje Vam očuvanje pčela unutar košnice.',
    image: 'hive-product.png',
    price: '120.00',
    smallImages: ['hive-product.png', 'hive-product.png', 'hive-product.png', 'hive-product.png']
  }
] 
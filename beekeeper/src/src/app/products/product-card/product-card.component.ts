import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() products: any[] = [];



  @Output() productValue = new EventEmitter();

  constructor() { }

  ngOnInit(): void {

  }

  public addProduct(event: any) {
    this.productValue.emit(event);
  }

}
